Introduction
============

This is the Zerg BLAST parsing library. It provides the
parsing engine used by the Perl module libzerg-perl.

Documentation
=============

Please refer to manpages Zerg and Zerg::Report in the module
libzerg-perl and to example programs in the package zerg-benchmarks.

Changes
=======

1.0.7 - 2009/01/27
 - Fixed YY_BUF_SIZE in Makefile. Should be 2*YY_READ_BUF_SIZE.
 - Fix: extra spaces after "Expect = " are now allowed. Bug reported by Laurent Manchon.
 - Fix: "make" now aborts when flex is absent.

1.0.6 - 2007/08/10
 - Added support for NCBI BLAST 2.2.15 and newer.
 - NCBI BLAST 2.2.14 **NOT** supported.
 - Added -fPIC option to enable compilation in 64-bit machines.
 
1.0.5 - 2005/01/10
 - Fix: added zerg_close_file(). Bug reported by Marcos Renato Araujo.

1.0.3 - 2003/10/29
 - fixed a dangling pointer bug in _zerg_strip_double_spaces()
 - changed yy_current_buffer to YY_CURRENT_BUFFER in zerg_get_token_offset() in order to compile with flex 2.5.31

1.0.0 - 2002/08/21	
 - original version


License terms
=============

Copyright (C) 2002  Apu�Paquola - Instituto de Qu�mica -
                    Universidade de S�o Paulo - Brasil

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program (see COPYING file); if not, write to the Free
Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
02111-1307 USA


Installation
============

First, make sure that GNU flex is installed in your system.

Then, unpack libzerg.tar.gz:

  gunzip -c libzerg.tar.gz | tar xf -

and:

  make

Then, as root:

  make install

This will create the directory /usr/local/libzerg. Next,
provide symbolic links to the installed files:

  ln -s /usr/local/libzerg/lib/libzerg.a /usr/local/lib/libzerg.a
  ln -s /usr/local/libzerg/include/zerg.h /usr/local/include/zerg.h
